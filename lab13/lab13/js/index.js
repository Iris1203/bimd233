function zeropad(num, size) {
var s = "000000000" + num;
return s.substr(s.length - size);
}

function Flight(airline, number, origin, destination, dep_time, arrival_time, arrival_gate, flightduration) {
  this.airline = airline;
  this.number = number;
  this.origin = origin;
  this.destination = destination;
  this.dep_time = dep_time;
  this.arrival_time = arrival_time;
  this.arrival_gate = arrival_gate;
  this.flightduration = flightduration;
    
};
var flight1= new Flight("Alaska", "B737", "Seattle-Tacoma Intl", "Fairbanks Intl", "Nov 11, 2018 18:00:0", "Nov 12, 2018 13:45:0'", "gate 1", "17:45:00");

var flight2= new Flight("Alaska", "B738", "Los Angeles Intl", "John F Kennedy Intl", "Nov 13, 2018 21:35:0", "Nov 14, 2018 05:41:0", "gate 8", "8:51:00");

var flight3= new Flight("Alaska", "B739", "Anchorage Intl", "Seattle-Tacoma Intl", "Nov 13, 2018 23:45:0", "Nov 14, 2018 04:12:0", "gate 12", "4:27:00");

var flight4= new Flight("Alaska", "A320", "San Francisco Intl", "Boston Logan Intl", "'Nov 13, 2018 21:05:0'", "'Nov 14, 2018 05:36:0'", "gate 9", "9:31:00");

var flight5= new Flight("United", "B752", "NEWARK", "LOS ANGELES", "'Nov 14, 2018 07:59:0'", "'Nov 14, 2018 11:04:0'", "gate 4", "3:05:00");

document.getElementById("Airline1").innerHTML=flight1.airline;
document.getElementById("Number1").innerHTML=flight1.number;
document.getElementById("Origin1").innerHTML=flight1.origin;
document.getElementById("Destination1").innerHTML=flight1.destination;
document.getElementById("Dep_time1").innerHTML=flight1.dep_time;
document.getElementById("Arrival_time1").innerHTML=flight1.arrival_time;
document.getElementById("Arrival_gate1").innerHTML=flight1.arrival_gate;
document.getElementById("Flightduration1").innerHTML=flight1.flightduration;

document.getElementById("Airline2").innerHTML=flight2.airline;
document.getElementById("Number2").innerHTML=flight2.number;
document.getElementById("Origin2").innerHTML=flight2.origin;
document.getElementById("Destination2").innerHTML=flight2.destination;
document.getElementById("Dep_time2").innerHTML=flight2.dep_time;
document.getElementById("Arrival_time2").innerHTML=flight2.arrival_time;
document.getElementById("Arrival_gate2").innerHTML=flight2.arrival_gate;
document.getElementById("Flightduration2").innerHTML=flight2.flightduration;

document.getElementById("Airline3").innerHTML=flight3.airline;
document.getElementById("Number3").innerHTML=flight3.number;
document.getElementById("Origin3").innerHTML=flight3.origin;
document.getElementById("Destination3").innerHTML=flight3.destination;
document.getElementById("Dep_time3").innerHTML=flight3.dep_time;
document.getElementById("Arrival_time3").innerHTML=flight3.arrival_time;
document.getElementById("Arrival_gate3").innerHTML=flight3.arrival_gate;
document.getElementById("Flightduration3").innerHTML=flight3.flightduration;

document.getElementById("Airline4").innerHTML=flight4.airline;
document.getElementById("Number4").innerHTML=flight4.number;
document.getElementById("Origin4").innerHTML=flight4.origin;
document.getElementById("Destination4").innerHTML=flight4.destination;
document.getElementById("Dep_time4").innerHTML=flight4.dep_time;
document.getElementById("Arrival_time4").innerHTML=flight4.arrival_time;
document.getElementById("Arrival_gate4").innerHTML=flight4.arrival_gate;
document.getElementById("Flightduration4").innerHTML=flight4.flightduration;

document.getElementById("Airline5").innerHTML=flight5.airline;
document.getElementById("Number5").innerHTML=flight5.number;
document.getElementById("Origin5").innerHTML=flight5.origin;
document.getElementById("Destination5").innerHTML=flight5.destination;
document.getElementById("Dep_time5").innerHTML=flight5.dep_time;
document.getElementById("Arrival_time5").innerHTML=flight5.arrival_time;
document.getElementById("Arrival_gate5").innerHTML=flight5.arrival_gate;
document.getElementById("Flightduration5").innerHTML=flight5.flightduration;