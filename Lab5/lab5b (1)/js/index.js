var canvas = document.getElementById("MyCanvas");
  var context = canvas.getContext("2d");
  context.moveTo(200, 0);
  context.lineTo(200, 600);
  context.moveTo(400, 0);
  context.lineTo(400, 600);
  context.moveTo(0, 200);
  context.lineTo(600, 200);
  context.moveTo(0, 400);
  context.lineTo(600, 400);
  context.lineWidth = 2;
  context.stroke();
  function drawCircle(x, y) {
    context.beginPath();
    var radius = 90,
      angle = 2 * Math.PI;
    context.arc(x, y, radius, 0, angle);
    context.lineWidth = 15;
    context.stroke();
  }
  drawCircle(100, 100);
  drawCircle(500, 500);

  context.moveTo(390, 390);
  context.lineTo(210, 210);
  context.moveTo(390, 210);
  context.lineTo(210, 390);
  context.stroke();
  context.beginPath();