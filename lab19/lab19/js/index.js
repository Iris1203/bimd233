const base_url = "https://api.weather.gov/stations/";
const endpoint = "/observations/latest";

// weather update button click
$("#getwx").on("click", function(e) {
  var mystation = $("input").val();
  var myurl = base_url + mystation + endpoint;
  $("input#my-url").val(myurl);

  // clear out any previous data
  $("ul li").each(function() {
    $(this).remove();
  });

  console.log("Cleared Elements of UL");

  // execute AJAX call to get and render data
  $.ajax({
    url: myurl,
    dataType: "json",
    success: function(data) {
      var rawMessage = data["properties"]["rawMessage"];
      var tempC = data["properties"]["temperature"].value.toFixed(1);
      var tempF = (tempC * 9 / 5 + 32).toFixed(1);
      var windDirection = data["properties"]["windDirection"].value.toFixed(1);
      var windSpeed = data["properties"]["windSpeed"].value.toFixed(1);
      var windS = (windSpeed * 1.94384).toFixed(1);
      var icon = data["properties"]["icon"];
      var textDescription = data["properties"]["textDescription"];

      // uncomment this if you want to dump full JSON to textarea
      var myJSON = JSON.stringify(data);
      $("textarea").val(myJSON);

      var str =
          "<li>" + rawMessage + "</li>" +
        "<li>Current temperature: " +
        tempC +
        "C " +
        tempF +
        "F" +
        "</li>" +
        "<li>Current wind: " +
        windDirection +
        "degrees at " +
        windS +
        "kts" +
        "</li>" +
        "<li> <img src='" +
        icon +
        "'>" + "</li>" + "<li>" +
        textDescription + "</li>";
      $("ul").append(str);
      $("ul li").attr("class", "list-group-item");
    }
  });
});