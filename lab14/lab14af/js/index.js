var day1 = ["Friday", "82", "55", "68.5"];

document.getElementById("tu1").innerHTML = day1[0];
document.getElementById("h1").innerHTML = day1[1];
document.getElementById("l1").innerHTML = day1[2];
document.getElementById("A1").innerHTML = day1[3];

var day2 = ["Saturday", "75", "52", "63.5"];

document.getElementById("tu2").innerHTML = day2[0];
document.getElementById("h2").innerHTML = day2[1];
document.getElementById("l2").innerHTML = day2[2];
document.getElementById("A2").innerHTML = day2[3];

var day3 = ["Sunday", "69", "52", "60.5"];

document.getElementById("tu3").innerHTML = day3[0];
document.getElementById("h3").innerHTML = day3[1];
document.getElementById("l3").innerHTML = day3[2];
document.getElementById("A3").innerHTML = day3[3];

var day4 = ["Monday", "69", "48", "58.5"];

document.getElementById("tu4").innerHTML = day4[0];
document.getElementById("h4").innerHTML = day4[1];
document.getElementById("l4").innerHTML = day4[2];
document.getElementById("A4").innerHTML = day4[3];

var day5 = ["Tuesday", "68", "51", "59.5"];

document.getElementById("tu5").innerHTML = day5[0];
document.getElementById("h5").innerHTML = day5[1];
document.getElementById("l5").innerHTML = day5[2];
document.getElementById("A5").innerHTML = day5[3];

var wx_data = [
  { day: "fri", hi: 82, lo: 55 },
  { day: "sat", hi: 75, lo: 52 },
  { day: "sun", hi: 69, lo: 52 },
  { day: "mon", hi: 69, lo: 48 },
  { day: "tue", hi: 68, lo: 51 }
];

var highData = [];
var lowData = [];

function sumHigh(total, num) {
  return total + num.hi;
}

function sumLow(total, num) {
  return total + num.lo;
}

function sumAll(total, num) {
  return total + num.hi + num.lo;
}
document.getElementById("AvHigh").innerHTML = wx_data.reduce(sumHigh, 0)/ wx_data.length + " degrees";

document.getElementById("AvLow").innerHTML = wx_data.reduce(sumLow, 0)/ wx_data.length + " degrees";