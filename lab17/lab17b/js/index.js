$(document).ready(function() {
  var book1 = { title: '1. "Seveneves"', author: "Neal Stephenson" };
  var book2 = { title: '2. "How Not to be Wrong"', author: "Jordan Ellenberg" };
  var book3 = { title: '3. "The Vital Question"', author: "Nick Lane" };
  var book4 = {
    title: '4. "The Power to Compete"',
    author: "Ryoichi Mikitani and Hiroshi Mikitani"
  };
  var book5 = {
    title: '5. "Sapiens: A Brief History of Humankind"',
    author: "Noah Yaval Harai"
  };
  var books = new Array();
  books.push(book1);
  books.push(book2);
  books.push(book3);
  books.push(book4);
  books.push(book5);
  var img_ref = {
    url:
      "http://www.gannett-cdn.com/-mm-/dcc982e8ed28edbe24141c93759ed7448c9e2642/c=58-0-965-682&r=x404&c=534x401/local/-/media/2016/05/20/USATODAY/USATODAY/635993645233133294-EPA-USA-WORLD-BANK-IMF-SPRING-MEETINGS-81292935.JPG",
    src: "http://bit.ly/1RiIMVl",
    alt: "Bill Gates",
    height: 100,
    width: 133
  };
  var reference = {
    url:
   "http://www.usatoday.com/story/money/markets/2016/05/20/bill-gates-read-these-5-books-summer/84675556/",
    src: "http://usat.ly/20hirO3",
    alt: "Gates Books",
    text: "BG:5 Books for Summer 2016"
  };
//$("div #bg img").attr("img_ref", "http://www.gannett-cdn.com/-mm-/dcc982e8ed28edbe24141c93759ed7448c9e2642/c=58-0-965-682&r=x404&c=534x401/local/-/media/2016/05/20/USATODAY/USATODAY/635993645233133294-EPA-USA-WORLD-BANK-IMF-SPRING-MEETINGS-81292935.JPG");
  $("div #bg img").attr(img_ref);
  //$("div #bg img").attr("alt", "Bill Gates");
 // $("div #bg img").css("width", "133");
 // $("div #bg img").css("height", "100");
  
  $("div #b5 a").attr("href", "http://usat.ly/20hirO3");
  $("div #b5 a").attr("alt", "Gates Books");
  $("div #b5 a").text("BG:5 Books for Summer 2016");
  $("ol").addClass("list-group");
  $("li").addClass("list-group-item");
  $("li").each(function(i) {
    this.innerText = books[i].title + " by " + books[i].author;
  });
  $("li").each(function(i) {
    $("li:odd").css("background", "lightgray");
  });
});