$(document).ready(function() {
  $(".panel").css("border", "5px dashed red");
  $(".panel").css("padding", "10px");
  $("div").css("padding", "3px");
  $(".cat").css("color", "green");
  $(".cat").css("background-color", "gray");
  $(".dog").css("background-color", "gray");
  $(".dog").css("color", "red");
  $("#lab").css("border", "1px solid yellow");
  $(".dog:last").css("background-color", "yellow");
  $("#calico").css("width", "50%");
  $("#calico").css("background-color", "green");
  $("#calico").css("color", "white");
});