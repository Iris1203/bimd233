var radius1 = 20;
var radius2 = 10;
var radius3 = 80;

var circle1 = calcCircleGeometries(radius1);
console.log(circle1);

var circle2 = calcCircleGeometries(radius2);
console.log(circle2);

var circle3 = calcCircleGeometries(radius3);
console.log(circle3);

function calcCircleGeometries(radius) {
  const pi = Math.PI;
  var area = pi * radius * radius;
  var circumference = 2 * pi * radius;
  var diameter = 2 * radius;
  var geometries = [area, circumference, diameter];
  return geometries;
}
document.getElementById("area1").innerHTML = circle1[0];
document.getElementById("circumference1").innerHTML = circle1[1];
document.getElementById("diameter1").innerHTML = circle1[2];

document.getElementById("area2").innerHTML = circle2[0];
document.getElementById("circumference2").innerHTML = circle2[1];
document.getElementById("diameter2").innerHTML = circle2[2];

document.getElementById("area3").innerHTML = circle3[0];
document.getElementById("circumference3").innerHTML = circle3[1];
document.getElementById("diameter3").innerHTML = circle3[2];