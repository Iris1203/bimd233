var car1= ['Audi', 'A8', '2017', '$49,500']

document.getElementById("make1").innerHTML= car1[0];
document.getElementById("model1").innerHTML= car1[1];
document.getElementById("year1").innerHTML= car1[2];
document.getElementById("price1").innerHTML= car1[3];

var car2= ['BMW', 'M2', '2017', '$45,500']

document.getElementById("make2").innerHTML= car2[0];
document.getElementById("model2").innerHTML= car2[1];
document.getElementById("year2").innerHTML= car2[2];
document.getElementById("price2").innerHTML= car2[3];

var car3= ['Jeep', 'Cherokee', '2017', '$19,740']

document.getElementById("make3").innerHTML= car3[0];
document.getElementById("model3").innerHTML= car3[1];
document.getElementById("year3").innerHTML= car3[2];
document.getElementById("price3").innerHTML= car3[3];

var car4= ['Ferrari', '348', '1991', '$48,999']

document.getElementById("make4").innerHTML= car4[0];
document.getElementById("model4").innerHTML= car4[1];
document.getElementById("year4").innerHTML= car4[2];
document.getElementById("price4").innerHTML= car4[3];

var car5= ['Honda', 'Accord', '2013', '$12,200']

document.getElementById("make5").innerHTML= car5[0];
document.getElementById("model5").innerHTML= car5[1];
document.getElementById("year5").innerHTML= car5[2];
document.getElementById("price5").innerHTML= car5[3];