var stocks = new Array();
stocks.push({
  logo: "https://x.pac-12.com/sites/default/files/styles/thumbnail/public/washington-state-logo__1438812470.png",
  School: "Washington State",
  Conference: "7-1",
  Overall: "10-1",
  LastGame: "W 69-28 ARIZ"
});
stocks.push({
  logo: "https://x.pac-12.com/sites/default/files/styles/thumbnail/public/washington-logo_0__1438812441.png",
  School: "Washington",
  Conference: "6-2",
  Overall: "8-3",
  LastGame: "W 42-23 ORST"
});
stocks.push({
  logo: "https://x.pac-12.com/sites/default/files/styles/thumbnail/public/stanford-logo-color_0.png",
  School: "Stanford",
  Conference: "4-3",
  Overall: "6-4",
  LastGame: "W 48-17 ORST"
});
stocks.push({
  logo: "https://x.pac-12.com/sites/default/files/styles/thumbnail/public/oregon-g-logo__1438812094.png",
  School: "Oregon",
  Conference: "4-4",
  Overall: "7-4",
  LastGame: "W 31-29 ASU"
});
stocks.push({
 logo: "https://x.pac-12.com/sites/default/files/styles/thumbnail/public/cal_yellow_lg_2017.png",
  School: "California",
  Conference: "3-4",
  Overall: "6-4",
  LastGame: "W 15-14 USC"
});
stocks.push({
 logo: "https://x.pac-12.com/sites/default/files/styles/thumbnail/public/oregon-state-logo_0__1438812147.png",
  School: "Oregon State",
  Conference: "1-7",
  Overall: "2-9",
  LastGame: "L 23-42 WASH"
});
var elements = document.getElementById("table");
function placeData(teams, index) {
  elements.innerHTML +=
    "<tr><td>" +
    '<img src="'+ teams.logo +'">'+
    "</th><td>" +
    teams.School +
    "</th><td>" +
    teams.Conference +
    "</th><td>" +
    teams.Overall +
    "</td><td>" +
    teams.LastGame +
    "</td><td>";
}