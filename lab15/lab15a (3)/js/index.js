var stocks = new Array();
stocks.push({
  Rank: "1",
  name: "Microsoft",
  ROIC: "roic: 20.6%",
  cap: "Market cap: $381.7B",
  FreeCashFlow: "freecashflow: $26.7B",
  sales: "Sale price: $86.8",
  profit: "Profit: $22.1B",
  profitloss: "Profitloss%: 1%",
  ROA: "Roa: 14%",
  employees: 128000
});
stocks.push({
  Rank: "2",
  name: "Symetra Financial",
  ROIC: "roic: 6.8%",
  cap: "Market cap: $2.7B",
  FreeCashFlow: "freecashflow: $978.3M",
  sales: "Sale price: $2.2B",
  profit: "Profit: $254.4M",
  profitloss: "Profitloss%: 15.3%",
  ROA: "Roa: 0.8%",
  employees: 1400
});
stocks.push({
  Rank: "3",
  name: "Micron Technology",
  ROIC: "roic: 20.7%",
  cap: "Market cap: $37.6",
  FreeCashFlow: "freecashflow: $3.0B",
  sales: "Sale price: $16.4",
  profit: "Profit: $3.0B",
  profitloss: "Profitloss%: 155.9%",
  ROA: "Roa: 14.6%",
  employees: 30400
});
stocks.push({
  Rank: "4",
  name: "F5 Networks",
  ROIC: "roic: 23.7%",
  cap: "Market cap: $9.5B",
  FreeCashFlow: "freecashflow: $526.3M",
  sales: "Sale price: $1.7B",
  profit: "Profit: $311.2M",
   profitloss: "Profitloss%: 12.2%",
  ROA: "Roa: 14.1%",
  employees: 3834
});
stocks.push({
  Rank: "5",
  name: "Expedia",
  ROIC: "roic: 9.3%",
  cap: "Market cap: $10.8B",
  FreeCashFlow: "freecashflow: $1.0B",
  sales: "Sale price: $5.8B",
  profit: "Profit: $398.1M",
   profitloss: "Profitloss%: 71%",
  ROA: "Roa: 4.8%",
  employees: 18210
});
stocks.push({
  Rank: "6",
  name: "Nautilus",
  ROIC: "roic: 35.9%",
  cap: "Market cap: $476M",
  FreeCashFlow: "freecashflow: $31.2M",
  sales: "Sale price: $274.4M",
  profit: "Profit: $18.8M",
   profitloss: "Profitloss%: -60.8%",
  ROA: "Roa: 11.8%",
  employees: 340
});
stocks.push({
  Rank: "7",
  name: "Heritage Financial",
  ROIC: "roic: 8.8%",
  cap: "Market cap: $531M",
  FreeCashFlow: "freecashflow: $47.4M",
  sales: "Sale price: $137.6M",
  profit: "Profit: $21M",
   profitloss: "Profitloss%: 119.5%",
  ROA: "Roa: 0.8%",
  employees: 748
});
stocks.push({
  Rank: "8",
  name: "Cascade Microtech",
  ROIC: "roic: 11.3%",
  cap: "Market cap: $239M",
  FreeCashFlow: "freecashflow: $19.3M",
  sales: "Sale price: $136M",
  profit: "Profit: $9.9M",
   profitloss: "Profitloss%: -26%",
  ROA: "Roa: 8%",
  employees: 449
});
stocks.push({
  Rank: "9",
  name: "Nike",
  ROIC: "roic: 19%",
  cap: "Market cap: $83.1B",
  FreeCashFlow: "freecashflow: $2.1B",
  sales: "Sale price: $27.8B",
  profit: "Profit: $2.7B",
   profitloss: "Profitloss%: 8.9%",
  ROA: "Roa: 14.9%",
  employees: 56500
});
stocks.push({
  Rank: "10",
  name: "Alaska Air Group",
  ROIC: "roic: 14.9%",
  cap: "Market cap: $7.9B",
  FreeCashFlow: "freecashflow: $336M",
  sales: "Sale price: $5.4B",
  profit: "Profit: $605M",
   profitloss: "Profitloss%: 19.1%",
  ROA: "Roa: 10.1%",
  employees: 13652
});

var elements = document.getElementById("table");
function placeData(stocks, index) {
  elements.innerHTML +=
    "<tr><th>" +
    stocks.Rank +
     "<th><td>" +
    stocks.name +
    "</th><td>" +
     stocks.ROIC +
    "</th><td>" +
    stocks.cap +
    "</td><td>" +
    stocks.FreeCashFlow +
    "</td><td>" +
    stocks.sales +
    "</td><td>" +
    stocks.profit +
    "</td><td>" +
    stocks.profitloss+
    "</td><td>" +
    stocks.ROA +
    "</td><td>" +
    stocks.employees +
    "</td></tr>";
}